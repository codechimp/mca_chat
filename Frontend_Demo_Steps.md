# Feathers.js Chat Demo - Frontend

## PRE-Pre-setup steps
Ensure Node v10.0.0 or higher is installed.

## Setting up the project
1) Run:
```
npm init react-app chat-fe
```
The:
```
npm i --save @feathersjs/client moment socket.io-client react-bootstrap bootstrap@3
```
Then:
```
cd chat-fe
npm start
```
And it should open a browser to [http://localhost:3000](http://localhost:3000) with the default React App page

## Creating UI
1) Create the following files:
  - `/src/login.js`
  - `/src/chat.js`
  - //src/feathers.js`
2) Paste the following in `/src/feathers.js`:
```javascript
import io from "socket.io-client";
import feathers from "@feathersjs/client";

const socket = io("http://localhost:3030");
const client = feathers();

client.configure(feathers.socketio(socket));
client.configure(
  feathers.authentication({
    storage: window.localStorage
  })
);

export default client;
```
3) Paste the following in `/src/login.js`:
```javascript
import React, { Component } from "react";
import client from "./feathers";

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  updateField(name, ev) {
    this.setState({ [name]: ev.target.value });
  }

  login() {
    const { email, password } = this.state;

    return client
      .authenticate({
        strategy: "local",
        email,
        password
      })
      .catch(error => this.setState({ error }));
  }

  signup() {
    const { email, password } = this.state;

    return client
      .service("users")
      .create({ email, password })
      .then(() => this.login());
  }

  render() {
    return (
      <main className="login container">
        <div className="row">
          <div className="col-12 col-6-tablet push-3-tablet text-center heading">
            <h1 className="font-100">Log in or signup</h1>
            <p>{this.state.error && this.state.error.message}</p>
          </div>
        </div>
        <div className="row">
          <div className="col-12 col-6-tablet push-3-tablet col-4-desktop push-4-desktop">
            <form className="form">
              <fieldset>
                <input
                  className="block"
                  type="email"
                  name="email"
                  placeholder="email"
                  onChange={ev => this.updateField("email", ev)}
                />
              </fieldset>

              <fieldset>
                <input
                  className="block"
                  type="password"
                  name="password"
                  placeholder="password"
                  onChange={ev => this.updateField("password", ev)}
                />
              </fieldset>

              <button
                type="button"
                className="button button-primary block signup"
                onClick={() => this.login()}
              >
                Log in
              </button>

              <button
                type="button"
                className="button button-primary block signup"
                onClick={() => this.signup()}
              >
                Signup
              </button>
            </form>
          </div>
        </div>
      </main>
    );
  }
}
```
3) Paste the following in `/src/chat.js`:
```javascript
import React, { Component } from "react";
import moment from "moment";
import client from "./feathers";

class Chat extends Component {
  sendMessage(ev) {
    const input = ev.target.querySelector('[name="text"]');
    const text = input.value.trim();

    if (text) {
      client
        .service("messages")
        .create({ text })
        .then(() => {
          input.value = "";
        });
    }

    ev.preventDefault();
  }

  scrollToBottom() {
    const chat = this.chat;

    chat.scrollTop = chat.scrollHeight - chat.clientHeight;
  }

  componentDidMount() {
    this.scrollToBottom = this.scrollToBottom.bind(this);

    client.service("messages").on("created", this.scrollToBottom);
    this.scrollToBottom();
  }

  componentWillUnmount() {
    // Clean up listeners
    client.service("messages").removeListener("created", this.scrollToBottom);
  }

  render() {
    const { users, messages } = this.props;

    return (
      <main className="flex flex-column">
        <header className="title-bar flex flex-row flex-center">
          <div className="title-wrapper block center-element">
            <img
              className="logo"
              src="http://feathersjs.com/img/feathers-logo-wide.png"
              alt="Feathers Logo"
            />
            <span className="title">Chat</span>
          </div>
        </header>

        <div className="flex flex-row flex-1 clear">
          <aside className="sidebar col col-3 flex flex-column flex-space-between">
            <header className="flex flex-row flex-center">
              <h4 className="font-300 text-center">
                <span className="font-600 online-count">{users.length}</span>{" "}
                users
              </h4>
            </header>

            <ul className="flex flex-column flex-1 list-unstyled user-list">
              {users.map(user => (
                <li key={user._id}>
                  <a className="block relative" href="#">
                    <img
                      src={user.avatar}
                      alt={user.email}
                      className="avatar"
                    />
                    <span className="absolute username">{user.email}</span>
                  </a>
                </li>
              ))}
            </ul>
            <footer className="flex flex-row flex-center">
              <a
                href="#"
                onClick={() => client.logout()}
                className="button button-primary"
              >
                Sign Out
              </a>
            </footer>
          </aside>

          <div className="flex flex-column col col-9">
            <main
              className="chat flex flex-column flex-1 clear"
              ref={main => {
                this.chat = main;
              }}
            >
              {messages.map(message => (
                <div key={message._id} className="message flex flex-row">
                  <img
                    src={
                      message.user && message.user.avatar
                        ? message.user.avatar
                        : "unknown.jpg"
                    }
                    alt={
                      message.user && message.user.email
                        ? message.user.email
                        : "NONE"
                    }
                    className="avatar"
                  />
                  <div className="message-wrapper">
                    <p className="message-header">
                      <span className="username font-600">
                        {message.user && message.user.email
                          ? message.user.email
                          : "NONE"}
                      </span>
                      <span className="sent-date font-300">
                        {moment(message.createdAt).format("MMM Do, hh:mm:ss")}
                      </span>
                    </p>
                    <p className="message-content font-300">{message.text}</p>
                  </div>
                </div>
              ))}
            </main>

            <form
              onSubmit={this.sendMessage.bind(this)}
              className="flex flex-row flex-space-between"
              id="send-message"
            >
              <input type="text" name="text" className="flex flex-1" />
              <button className="button-primary" type="submit">
                Send
              </button>
            </form>
          </div>
        </div>
      </main>
    );
  }
}

export default Chat;
```
4) Paste the following into `/src/App.js`:
```javascript
import React, { Component } from "react";
import Login from "./login";
import Chat from "./chat";
import client from "./feathers";

class Application extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    const messages = client.service("messages");
    const users = client.service("users");

    // Try to authenticate with the JWT stored in localStorage
    client.authenticate().catch(() => this.setState({ login: null }));

    // On successfull login
    client.on("authenticated", login => {
      // Get all users and messages
      Promise.all([
        messages.find({
          query: {
            $sort: { createdAt: -1 },
            $limit: 25
          }
        }),
        users.find()
      ]).then(([messagePage, userPage]) => {
        // We want the latest messages but in the reversed order
        const messages = messagePage.reverse();
        const users = userPage.data;

        // Once both return, update the state
        this.setState({ login, messages, users });
      });
    });

    // On logout reset all all local state (which will then show the login screen)
    client.on("logout", () =>
      this.setState({
        login: null,
        messages: null,
        users: null
      })
    );

    // Add new messages to the message list
    messages.on("created", message =>
      this.setState({
        messages: this.state.messages.concat(message)
      })
    );

    // Add new users to the user list
    users.on("created", user =>
      this.setState({
        users: this.state.users.concat(user)
      })
    );
  }

  render() {
    if (this.state.login === undefined) {
      return (
        <main className="container text-center">
          <h1>Loading...</h1>
        </main>
      );
    } else if (this.state.login) {
      return <Chat messages={this.state.messages} users={this.state.users} />;
    }

    return <Login />;
  }
}

export default Application;
```
5) Paste the following into `/src/index.js`:
```javascript
import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

ReactDOM.render(<App />, document.getElementById("app"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
```
6) Paste the following to `/public/index.html`:
```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <link rel="shortcut icon" href="%PUBLIC_URL%/favicon.ico" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0"
    />
    <meta name="theme-color" content="#000000" />
    <meta
      name="description"
      content="Web site created using create-react-app"
    />
    <link rel="apple-touch-icon" href="%PUBLIC_URL%/logo192.png" />
    <!--
      manifest.json provides metadata used when your web app is installed on a
      user's mobile device or desktop. See https://developers.google.com/web/fundamentals/web-app-manifest/
    -->
    <link rel="manifest" href="%PUBLIC_URL%/manifest.json" />
    <!--
      Notice the use of %PUBLIC_URL% in the tags above.
      It will be replaced with the URL of the `public` folder during the build.
      Only files inside the `public` folder can be referenced from the HTML.

      Unlike "/favicon.ico" or "favicon.ico", "%PUBLIC_URL%/favicon.ico" will
      work correctly both with client-side routing and a non-root public URL.
      Learn how to configure a non-root public URL by running `npm run build`.
    -->
    <link
      rel="stylesheet"
      href="//cdn.rawgit.com/feathersjs/feathers-chat/v0.2.0/public/base.css"
    />
    <link
      rel="stylesheet"
      href="//cdn.rawgit.com/feathersjs/feathers-chat/v0.2.0/public/chat.css"
    />
    <title>MCA Chat!</title>
  </head>
  <body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="app" class="flex flex-column"></div>
    <!--
      This HTML file is a template.
      If you open it directly in the browser, you will see an empty page.

      You can add webfonts, meta tags, or analytics to this file.
      The build step will place the bundled scripts into the <body> tag.

      To begin the development, run `npm start` or `yarn start`.
      To create a production bundle, use `npm run build` or `yarn build`.
    -->
  </body>
</html>
```

## Fixing the "unknown image" issue:
1) Open `/src/chat.js`
2) Change lines 69-73:
```javascript
                    <img
                      src={user.avatar}
                      alt={user.email}
                      className="avatar"
                    />
```
To:
```javascript
                    <img
                      src={user.avatar ? user.avatar : "logo192.png"}
                      alt={user.email}
                      className="avatar"
                    />
````
3) Change lines 100-104:
```javascript
                    src={
                      message.user && message.user.avatar
                        ? message.user.avatar
                        : "unknown.jpg"
                    }
```
To:
```javascript
                    src={
                      message.user && message.user.avatar
                        ? message.user.avatar
                        : "logo192.png"
                    }
```

# Notes
 - It will fail when connecting outside of "localhost".  You need to modify `/src/feathers.js` in the UI code to set the socket to the IP/hostname instead of "localhost"