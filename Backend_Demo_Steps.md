# Feathers.js Chat Demo - Backend

## PRE-Pre-setup steps
Ensure Node v10.0.0 or higher is installed.
Install Feathers.js globally: `npm install @feathersjs/cli -g`

## Setting up the project
1) Run:
```
mkdir chat_be
cd chat_be
feathers generate app
```
2) Answer the following questions:
```
? Do you want to use JavaScript or TypeScript? JavaScript
? Project name mca-chat-be
? Description MCA Chat
? What folder should the source files live in? src
? Which package manager are you using (has to be installed globally)? npm
? What type of API are you making? (Press <space> to select, <a> to toggle all,
<i> to invert selection)REST, Realtime via Socket.io
? Which testing framework do you prefer? Jest
? This app uses authentication Yes
? What authentication strategies do you want to use? (See API docs for all 180+
supported oAuth providers) (Press <space> to select, <a> to toggle all, <i> to i
nvert selection)Username + Password (Local)
? What is the name of the user (entity) service? users
? What kind of service is it? NeDB
? What is the database connection string? nedb://../data
```

At this point you can run `npm run dev` and visit [http://localhost:3030](http://localhost:3030) and see the default Feathers page (found in /public in the project folder)

## Creating Messages Service
1) Create folder: `/src/services/messages`
2) Create the following files in `/src/services/messages`
  - `messages.class.js`
  - `messages.service.js`  
3) Paste the following in `/src/services/messages/messages.class.js`:
```
exports.Messages = class Messages {
  constructor() {
    this.messages = [];
  }

  async find() {
    // Just return all our messages
    return this.messages;
  }

  async create(data) {
    // The new message is the data merged with a unique identifier
    // using the messages length since it changes whenever we add one
    const message = {
      id: this.messages.length,
      text: data.text
    };

    // Add new message to the list
    this.messages.push(message);

    return message;
  }
};
```
4) PAste the following in `/src/services/messages/messages.service.js`:
```
// Initializes the `users` service on path `/users`
const { Messages } = require('./messages.class');

module.exports = function(app) {
  // Register the message service on the Feathers application
  app.use('/messages', new Messages());

  // Log every time a new message has been created
  app.service('messages').on('created', message => {
    console.log('A new message has been created', message);
  });

  // A function that creates new messages and then logs
  // all existing messages
  const main = async () => {
    // Create a new message on our message service
    await app.service('messages').create({
      text: 'Hello Feathers'
    });

    await app.service('messages').create({
      text: 'Hello again'
    });

    // Find all existing messages
    const messages = await app.service('messages').find();

    console.log('All messages', messages);
  };

  main();
};
```
5) Modify `/src/services/index.js`:
```
const users = require('./users/users.service.js');
const messages = require('./messages/messages.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function(app) {
  app.configure(users);
  app.configure(messages);
};

```
6) Test by running `npm run dev`...should see something like:
```
~ npm run dev

> mca-chat-be@0.0.0 dev /Users/mbauer/development/demos/mca_chat/test_run1/chat_be
> nodemon src/

[nodemon] 1.19.4
[nodemon] to restart at any time, enter `rs`
[nodemon] watching dir(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `node src/`
Publishing all events to all authenticated users. See `channels.js` and https://docs.feathersjs.com/api/channels.html for more information.
A new message has been created { id: 0, text: 'Hello Feathers' }
Publishing all events to all authenticated users. See `channels.js` and https://docs.feathersjs.com/api/channels.html for more information.
A new message has been created { id: 1, text: 'Hello again' }
All messages [ { id: 0, text: 'Hello Feathers' },
  { id: 1, text: 'Hello again' } ]
info: Feathers application started on http://localhost:3030
```
Then go to [http://localhost:3030/messages](http://localhost:3030/messages) and you should see this:
```
[{"id":0,"text":"Hello Feathers"},{"id":1,"text":"Hello again"}]
```

**At this point, we should make the UI part**


## Adding Realtime API
Modify the `/src/services/messages/messages.service.js` to:
```
// Initializes the `users` service on path `/users`
const { Messages } = require('./messages.class');

module.exports = function(app) {
  // Register the message service on the Feathers application
  app.use('/messages', new Messages());

  // Log every time a new message has been created
  app.service('messages').on('created', message => {
    console.log('A new message has been created', message);
  });

  // Add any new real-time connection to the `everybody` channel
  app.on('connection', connection => app.channel('everybody').join(connection));
  // Publish all events to the `everybody` channel
  app.publish(data => app.channel('everybody'));

  // For good measure let's create a message
  // So our API doesn't look so empty
  app.service('messages').create({
    text: 'Hello world from the server'
  });
};

```